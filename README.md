# README #

### ¿Qué es Feedbacker for WP? ###

* Feedbacker for WP permite activar Feedbacker en nuestro proyecto WP sin necesidad de modificar el código.
* 1.0
* Autor: [Borja Herrero](https://twitter.com/Piesblancos)

### Instalación ###

* Sube el fichero feedbacker-for-wp.php al directorio mu-plugins de tu instalación de WP, de ésta manera el usuario no tendrá acceso a la configuración.
* En el panel Herramientas aparecerá una nueva subpágina llamada "Feedbacker for WP", introduce los datos relativos al panel y listado.
* Activa o desactiva según necesidad

### Requirimientos ###

* Para poder utilizar Feedbacker for WP necesitarás una cuenta de Trello
* Crear un tablero específico para que se creen las notificaciones
* Crear un listado específico para el mismo fin

### Feedbacker ###

* Feedbacker es una herramienta desarrollada para Artvisual por Paco Valls