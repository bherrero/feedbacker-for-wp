<?php
/**
* Plugin Name: Feedbacker for WP
* Plugin URI: 
* Description: Plugin para instalar y activar Feedbacker en WP
* Version: 1.0.0
* Author: Borja Herrero
* Author URI: https://github.com/bherrero
* License: GPL2
*/


/**
 * ACTIONS
 */
add_action( 'admin_menu', 'tailorcode_register_custom_submenu_page');
add_action( 'admin_init', 'tailorcode_register_settings' );


/**
 * FILTERS
 */
// Plugin Settings link
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'add_action_link' );
// Scripts
add_filter( 'wp_enqueue_scripts', 'tailorcode_feedbacker_scripts' );
// Data Attributes
add_filter('script_loader_tag', 'tailorcode_add_data_attributes', 10, 2);



/**
 * FUNCTIONS
 */

// Add page to admin menu
function tailorcode_register_custom_submenu_page() {
    add_submenu_page(
        'tools.php',
        'Feedbacker for WP',
        'Feedbacker for WP',
        'manage_options',
        plugin_basename( __FILE__ ),
        'tailorcode_custom_submenu_page_callback' );
}
 
function tailorcode_custom_submenu_page_callback() { 
?>
    <div class="wrap">
    <h2>Feedbacker for WP</h2>

    <form method="post" action="options.php">
        <?php settings_fields( plugin_basename( __FILE__ ) ); ?>
        <?php do_settings_sections( plugin_basename( __FILE__ ) ); ?>
        <table class="form-table">

			<tr valign="top">
            <th scope="row">Activar Feedbacker</th>
            <td><input type="checkbox" id="tailorcode_feedbacker_activate" name="tailorcode_feedbacker_activate" value="active" <?php checked( 'active', get_option( 'tailorcode_feedbacker_activate' ) ); ?> /></td>
            </tr>

            <tr valign="top">
            <th scope="row">Nombre del tablero</th>
            <td><input type="text" name="tailorcode_feedbacker_board_name" value="<?php echo esc_attr( get_option('tailorcode_feedbacker_board_name') ); ?>" /></td>
            </tr>
             
            <tr valign="top">
            <th scope="row">Nombre de la lista</th>
            <td><input type="text" name="tailorcode_feedbacker_list_name" value="<?php echo esc_attr( get_option('tailorcode_feedbacker_list_name') ); ?>" /></td>
            </tr>

            <tr>
            	<td colspan="2">Introduce los nombres del tablero y la lista tal y como aparecen en Trello (espacios y mayúsculas incluidas).</td>
            </tr>
        </table>
        
        <?php submit_button(); ?>

    </form>
    </div>
<?php
}

// Register Settings
function tailorcode_register_settings() {
	register_setting( plugin_basename( __FILE__ ), 'tailorcode_feedbacker_activate' );
	register_setting( plugin_basename( __FILE__ ), 'tailorcode_feedbacker_board_name' );
	register_setting( plugin_basename( __FILE__ ), 'tailorcode_feedbacker_list_name' );
}



// Add action link
function add_action_links( $links ) {
	$mylinks = array(
		'<a href="' . admin_url( 'tools.php?page=' . plugin_basename( __FILE__ ) ) . '">Configuración</a>',
	);
	
	return array_merge( $links, $mylinks );
}

// Add script
function tailorcode_feedbacker_scripts() {
	if( is_feedbacker_activated() )
		wp_enqueue_script( 'tc-feedbacker', 'http://feedback.artvisual.net/artvisualfeedbacker.min.js', array( 'jquery' ), '0.1', true );
}


// Add data attributes to the script
function tailorcode_add_data_attributes( $tag, $handle ) {
	if( is_feedbacker_activated() ) {
		if( 'tc-feedbacker' === $handle ) {
			echo 'FEEDBACKER CARGADO';

			$board_name = get_option( 'tailorcode_feedbacker_board_name' );
			$list_name  = get_option( 'tailorcode_feedbacker_list_name' );


			$data_attrs  = ' data-board="' . $board_name . '"';
			$data_attrs .= ' data-list="' . $list_name . '"';
			$data_attrs .= ' data-appkey="5df96ead084d75887f4b431d71c4ea3d"';
			$data_attrs .= ' data-appsecret="d411c4a9e3ac63230ec502a78c6285d6464f895969744c08ace41d9c8f29d766"';

			return str_replace( ' src', $data_attrs . ' src', $tag );
		}		
	}

	return $tag;
}

function is_feedbacker_activated() {
	return ( 'active' == get_option( 'tailorcode_feedbacker_activate' ) ) ? true : false;
}